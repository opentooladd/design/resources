$(document).ready(function() {
	var typography = {
		location: '/wp-content/thesis/typography/app.php',
		init: function() {
			$('#submit').click(function() {
				typography.calc({
					font: $('#font').val(),
					size: $('#size').val(),
					width: $('#width').val() });
				return false;
			});
			typography.toggle_boxes();
		},
		calc: function(type) {
			$.post(typography.location, type, function(html) {
				var controls = $('#controls').html();
				var values = {
					font: $('#controls #font').val(),
					size: $('#controls #size').val(),
					width: $('#controls #width').val() };
				if (!values.font && (values.size || values.width))
					values.font = 'Georgia';
				$('#calculator').children().remove();
				$('#calculator').append(html);
				if ($('.grt').hasClass('google'))
					WebFont.load({
						google: {
							families: [$('.grt').data('font')+': 400,400i,700'] } });
				$('#controls').append(controls);
				$('#controls #font').val(values.font);
				$('#controls #size').val(values.size);
				$('#controls #width').val(values.width);
				if ($('#links').length)
					typography.sample($('#links li:first a').attr('title'));
				$('#links li:first a').addClass('active');
				$('#links li a').on('click', function() {
					var sample = $(this).attr('title');
					$(this).parent().siblings().children('a').removeClass('active');
					$(this).addClass('active');
					typography.sample(sample);
					return false;
				});
				$('#css').on('focus', function(){
					$(this).select();
					$(this).on('mouseup', function() {
						$(this).unbind('mouseup');
						return false;
					});
				});
				typography.init();
			});
		},
		toggle_boxes: function() {
			$('.grt .toggle-box .toggle').on('click', function() {
				if ($(this).siblings('.toggle-content').is(':visible')) {
					$(this).siblings('.toggle-content').hide();
					$(this).children('.on').hide();
					$(this).children('.off').show();
					return false;
				}
				else {
					$(this).parent().siblings('.toggle-box').children('.toggle-content').each(function() {
						$(this).hide();
						$(this).siblings('.toggle').children('.on').hide();
						$(this).siblings('.toggle').children('.off').show();
					});
					$(this).siblings('.toggle-content').show();
					$(this).children('.on').show();
					$(this).children('.off').hide();
					if ($(this).parent('.toggle-box').is(':first-of-type'))
						return false;
					else
						typography.jump($(this).data('jump'));
				}
			});
		},
		jump: function(id) {
			$('html, body').animate({
				scrollTop: $(id).offset().top
			}, 0);
		},
		sample: function(sample) {
			$('#settings').removeClass();
			$('#settings').addClass(sample);
			$('#settings .results').hide();
			$('#settings .results.'+sample).show();
			$('.grt').attr('id', sample);
			var css = $('#css-seeds .'+sample).html();
			$('#css').html($.trim(css));
		}
	};
	typography.init();
});